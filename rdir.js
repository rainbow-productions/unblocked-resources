// This code sets the function to redirect visitors to the listed link.
function redirect(elementId, url) {
    const element = document.getElementById(elementId);
    element.onclick = function () {
        window.location.replace(url);
    }
}
// For the index.html page, to make sure they read the agreement.
function redirect_agreement() {
    alert('By clicking "OK" you agree to not tell any teacher about our website.');
    window.location.replace('resources')
}
// The links below will redirect visitors to the button that they clicked on.
redirect("jsexecBtn", "https://unblocked-resources.vercel.app/js-exec.html");
redirect("proxy1Btn", "https://ublres-prox1.vercel.app/Index.html");
redirect("proxysecBtn", "https://unblocked-resources.vercel.app/secret-proxy");
redirect("gamesBtn", "https://unblocked-games.s3.amazonaws.com/index.html");
redirect("webmkBtn", "https://gitlab.com");
redirect("webdpBtn", "https://vercel.com");
redirect("spotBtn", "https://open.spotify.com/");
redirect("ntshrBtn", "https://forms.gle/2Mw7QnZtuUn14u2E8")
redirect("calcBtn", "https://rainboweverything.vercel.app/rainbowcalculator/index.html");
redirect("chessBtn", "https://rainbowchess.vercel.app/")