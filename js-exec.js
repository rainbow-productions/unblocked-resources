// JavaScript code can be executed directly in the HTML file
// using the <script> tag. No additional JavaScript code is needed.
 
$(document).ready(function() {
    $('#scriptExecutorForm').submit(function(e) {
        e.preventDefault();
        var scriptCode = $('#scriptCode').val();
        try {
            var result = eval(scriptCode);
            $('#output').html('' + result + '');
        } catch (error) {
            $('#output').html('Error: ' + error + '');
        }
    });
});