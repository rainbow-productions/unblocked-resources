// _error.js
// error.js redirects the browser to a certain page if there's an error.
export default function Error({ statusCode }) {
    if (statusCode === 404) {
        window.location.replace('404.html')
    }
  
    if (statusCode === 403) {
      window.location.replace('403.html')
    }
  
    window.location.replace('uerror.html')
  }
  