// This code gives the short link for this website.
function linkgen(elementId, url) {

    const element = document.getElementById(elementId);
  
    element.onclick = async function() {
  
      await navigator.clipboard.writeText(url);
      
      const link = document.getElementById('link');
      link.innerHTML = 'Copied to clipboard!';
      
      setTimeout(() => {
        link.innerHTML = ''; 
      }, 1000);
  
    }
  
  }
  // The link
  linkgen("shareButton", "https://tinyurl.com/ublme");
  